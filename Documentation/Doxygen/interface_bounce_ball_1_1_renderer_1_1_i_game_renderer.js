var interface_bounce_ball_1_1_renderer_1_1_i_game_renderer =
[
    [ "BuildDrawing", "interface_bounce_ball_1_1_renderer_1_1_i_game_renderer.html#af5f9933ce2885b7532a9291d9971272d", null ],
    [ "GetBackground", "interface_bounce_ball_1_1_renderer_1_1_i_game_renderer.html#a489109fb1c38bac097b71d8d6e9c1314", null ],
    [ "GetBrush", "interface_bounce_ball_1_1_renderer_1_1_i_game_renderer.html#a017c28c5b98bf1ce40ffd198ccabfacd", null ],
    [ "GetCheckpoint", "interface_bounce_ball_1_1_renderer_1_1_i_game_renderer.html#ae0aa53faaba34e0b172ef32bb5a148d6", null ],
    [ "GetExit", "interface_bounce_ball_1_1_renderer_1_1_i_game_renderer.html#af604046abd5da833fec152a222b474c1", null ],
    [ "GetLifes", "interface_bounce_ball_1_1_renderer_1_1_i_game_renderer.html#aaeb65120afa8807facdf820626bab447", null ],
    [ "GetLives", "interface_bounce_ball_1_1_renderer_1_1_i_game_renderer.html#ae8873c6a4694d646fc61fa5f490a0014", null ],
    [ "GetPlayer", "interface_bounce_ball_1_1_renderer_1_1_i_game_renderer.html#a32ba091c52ac7b332c0bc4479941df11", null ],
    [ "GetRedBar", "interface_bounce_ball_1_1_renderer_1_1_i_game_renderer.html#a58a0a80e8c14253a79220f863cd4e5ff", null ],
    [ "GetRing", "interface_bounce_ball_1_1_renderer_1_1_i_game_renderer.html#addcca6bac6b16a2f9544e159c0d19593", null ],
    [ "GetScore", "interface_bounce_ball_1_1_renderer_1_1_i_game_renderer.html#ae176fa09519ea285641c8d6a9184c321", null ],
    [ "GetSpiders", "interface_bounce_ball_1_1_renderer_1_1_i_game_renderer.html#a187d0ed7ebfd67cd76df4563931e631d", null ],
    [ "GetWalls", "interface_bounce_ball_1_1_renderer_1_1_i_game_renderer.html#ae36312b0f17637e3bc9db115887d1d3e", null ],
    [ "Reset", "interface_bounce_ball_1_1_renderer_1_1_i_game_renderer.html#a5611572fbdd8cb2fb7b777a1963866be", null ]
];