var hierarchy =
[
    [ "Application", null, [
      [ "BounceBall.App", "class_bounce_ball_1_1_app.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "BounceBall.BallControl", "class_bounce_ball_1_1_ball_control.html", null ]
    ] ],
    [ "BounceBall.Tests.GameTests", "class_bounce_ball_1_1_tests_1_1_game_tests.html", null ],
    [ "IComponentConnector", null, [
      [ "BounceBall.ScoreBoard", "class_bounce_ball_1_1_score_board.html", null ],
      [ "BounceBall.ScoreDialog", "class_bounce_ball_1_1_score_dialog.html", null ]
    ] ],
    [ "BounceBall.Logic.IGameLogic", "interface_bounce_ball_1_1_logic_1_1_i_game_logic.html", [
      [ "BounceBall.Logic.BallLogic", "class_bounce_ball_1_1_logic_1_1_ball_logic.html", null ]
    ] ],
    [ "BounceBall.Data.IGameModel", "interface_bounce_ball_1_1_data_1_1_i_game_model.html", [
      [ "BounceBall.Data.BallModel", "class_bounce_ball_1_1_data_1_1_ball_model.html", null ]
    ] ],
    [ "BounceBall.Renderer.IGameRenderer", "interface_bounce_ball_1_1_renderer_1_1_i_game_renderer.html", [
      [ "BounceBall.Renderer.BallRenderer", "class_bounce_ball_1_1_renderer_1_1_ball_renderer.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "BounceBall.Logic.IScoreLogic", "interface_bounce_ball_1_1_logic_1_1_i_score_logic.html", [
      [ "BounceBall.Logic.ScoreLogic", "class_bounce_ball_1_1_logic_1_1_score_logic.html", null ]
    ] ],
    [ "BounceBall.Repository.IStorageRepository", "interface_bounce_ball_1_1_repository_1_1_i_storage_repository.html", [
      [ "BounceBall.Repository.StorageRepo", "class_bounce_ball_1_1_repository_1_1_storage_repo.html", null ]
    ] ],
    [ "BounceBall.Data.Score", "class_bounce_ball_1_1_data_1_1_score.html", null ],
    [ "BounceBall.Data.Spiders", "class_bounce_ball_1_1_data_1_1_spiders.html", null ],
    [ "Window", null, [
      [ "BounceBall.ScoreBoard", "class_bounce_ball_1_1_score_board.html", null ],
      [ "BounceBall.ScoreDialog", "class_bounce_ball_1_1_score_dialog.html", null ]
    ] ],
    [ "Window", null, [
      [ "BounceBall.MainMenuWindow", "class_bounce_ball_1_1_main_menu_window.html", null ],
      [ "BounceBall.MainWindow", "class_bounce_ball_1_1_main_window.html", null ]
    ] ]
];