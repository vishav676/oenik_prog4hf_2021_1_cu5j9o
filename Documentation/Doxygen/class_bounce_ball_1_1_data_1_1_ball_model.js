var class_bounce_ball_1_1_data_1_1_ball_model =
[
    [ "BallModel", "class_bounce_ball_1_1_data_1_1_ball_model.html#a968fd26c47f374b0fb41fedebc023276", null ],
    [ "MoveY", "class_bounce_ball_1_1_data_1_1_ball_model.html#a35fca7b04ac0c090cb8736e249c59390", null ],
    [ "Ball", "class_bounce_ball_1_1_data_1_1_ball_model.html#a96a024f805880268085f277d8eab5ccf", null ],
    [ "Bars", "class_bounce_ball_1_1_data_1_1_ball_model.html#a4a94aa586469909702beb5c695572862", null ],
    [ "Checkpoint", "class_bounce_ball_1_1_data_1_1_ball_model.html#a39821f2283536d12502621f670ce968b", null ],
    [ "Exit", "class_bounce_ball_1_1_data_1_1_ball_model.html#a03ce36961c7f5cd58d54ecf2e5c5bbba", null ],
    [ "GameHeight", "class_bounce_ball_1_1_data_1_1_ball_model.html#a59ff8f0357c5173810921db2006337a4", null ],
    [ "GameWidth", "class_bounce_ball_1_1_data_1_1_ball_model.html#a0b76bbde2f02b4f40c78fd80534ad67e", null ],
    [ "Lifes", "class_bounce_ball_1_1_data_1_1_ball_model.html#aed662e85427d2c093d2186645d0bc510", null ],
    [ "Lives", "class_bounce_ball_1_1_data_1_1_ball_model.html#ac86f58ca24c6d2838e4734c0f69f14fd", null ],
    [ "Ring", "class_bounce_ball_1_1_data_1_1_ball_model.html#a0b946ebe802860fb0d3fb5e41b757b15", null ],
    [ "Score", "class_bounce_ball_1_1_data_1_1_ball_model.html#a4bd5973b3afe4ba5d02e48aca62c5878", null ],
    [ "Spiders", "class_bounce_ball_1_1_data_1_1_ball_model.html#a9eb82b6ddee4f72a78900595d0e09c0d", null ],
    [ "TileSize", "class_bounce_ball_1_1_data_1_1_ball_model.html#a8f40f2e6c0e9e0948a1d4a1bbb41d793", null ],
    [ "Walls", "class_bounce_ball_1_1_data_1_1_ball_model.html#aaa38eda04ab71046dbb5f64462c9941b", null ]
];