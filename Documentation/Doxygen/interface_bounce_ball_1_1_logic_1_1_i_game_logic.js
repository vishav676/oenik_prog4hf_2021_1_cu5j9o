var interface_bounce_ball_1_1_logic_1_1_i_game_logic =
[
    [ "BarFound", "interface_bounce_ball_1_1_logic_1_1_i_game_logic.html#ad9cfcee62cf6a61f5afed66feaffd501", null ],
    [ "CalculateScore", "interface_bounce_ball_1_1_logic_1_1_i_game_logic.html#a99719c46ba991814f049f63cd9f54697", null ],
    [ "ExtraLive", "interface_bounce_ball_1_1_logic_1_1_i_game_logic.html#a73f24a3b1e80886304ca7161cef04001", null ],
    [ "IsGameOver", "interface_bounce_ball_1_1_logic_1_1_i_game_logic.html#ab7c4d76752b6450b6d86351f9af631d3", null ],
    [ "Jump", "interface_bounce_ball_1_1_logic_1_1_i_game_logic.html#acc3022af2c3fa30b516b5a30f8d016c6", null ],
    [ "Move", "interface_bounce_ball_1_1_logic_1_1_i_game_logic.html#afc3516fcb75f27640db67fa7fcad9ba5", null ],
    [ "MoveSpider", "interface_bounce_ball_1_1_logic_1_1_i_game_logic.html#a62919b7d9aaeee5a8bb63447eb4348a0", null ],
    [ "MoveToCheckpoint", "interface_bounce_ball_1_1_logic_1_1_i_game_logic.html#a4a69530673725859373826f7b1d8449f", null ],
    [ "ReachedCheckpoint", "interface_bounce_ball_1_1_logic_1_1_i_game_logic.html#aa6660e903803058b881be6e4a4584f9d", null ],
    [ "RingFound", "interface_bounce_ball_1_1_logic_1_1_i_game_logic.html#a71b7cf43532bcf6cf086c3e60087b5f0", null ],
    [ "SpiderFound", "interface_bounce_ball_1_1_logic_1_1_i_game_logic.html#ac3a4afa21235b27dd5dffc75a81a2f99", null ]
];