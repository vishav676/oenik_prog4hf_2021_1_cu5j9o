var class_bounce_ball_1_1_logic_1_1_ball_logic =
[
    [ "BallLogic", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#a2684140a888d15b9b61f74e4f31575e9", null ],
    [ "BarFound", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#a68c576aaddbd1fab6ccd7f1371a8eecf", null ],
    [ "CalculateScore", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#afcd6f3b0e34f32f820ee4544a2c304a1", null ],
    [ "ExtraLive", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#ab42229fd27dcab7616ad6053912352b3", null ],
    [ "GamePaused", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#add8063e41587c9eea1d10e3a520ec33e", null ],
    [ "IsGameOver", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#af5871f28be842f94c5a062fc9a55199c", null ],
    [ "Jump", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#ada43e571c785fa3fcd1eb1c8f842e317", null ],
    [ "Move", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#ac46ca7dfd220cf4a45df73d731aa17b6", null ],
    [ "MoveSpider", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#a69667203f6fccaa1d9f1b7a72fa311a6", null ],
    [ "MoveToCheckpoint", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#a98a1506c79dceaf0ccf05d3a580417f9", null ],
    [ "ReachedCheckpoint", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#adaca64f4dcc5d1309b2a0b8c8291d1d4", null ],
    [ "RingFound", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#a71ec93b79da444aea287f6bb0f3866cb", null ],
    [ "SpiderFound", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#a81b28594be8b4c328d66dcdc5cf98403", null ],
    [ "IsFalling", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#aaa2c267fa1cc58c630b613fbbf8c1bd9", null ],
    [ "IsJumping", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#a2264628b7a502d7a7df3e8893b7e7691", null ],
    [ "RefreshScreen", "class_bounce_ball_1_1_logic_1_1_ball_logic.html#a281fbd76ab030d0042344e6e5d58f8c3", null ]
];