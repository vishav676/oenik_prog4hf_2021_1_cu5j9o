﻿namespace EventManagement.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Class to store the result of the query.
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether query was succesful.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}
